var ready = false;

$(window).on("load", function() 
{
	if (!ready)
	{
		ready = true;
		$(".html5-game-thumbnail").hover(playVideo, playVideo);
		$(".html5-game-thumbnail-single").hover(playVideo, playVideo);
		$("#loading-bar").fadeOut(3000);
		$("#main-container").fadeIn(3000, function()
		{
			
		});
	}
	
});

var imageSrcSwap = function () 
{
    var $this = $(this);
    var newSource = $this.data('alt-src');
    $this.data('alt-src', $this.attr('src'));
    $this.attr('src', newSource);
}

var playVideo = function ()
{
	if (!this.paused)
	{
		this.pause();
	}
	else
	{
		this.play();
	}
}


